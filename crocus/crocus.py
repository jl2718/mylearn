#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 26 06:14:19 2017

@author: johnlakness
"""
# basic python version
import collections as co
import itertools as it
import math

fnExpand = lambda x: it.chain(*(it.combinations(x, i) for i in range(1, 2)))  # expand out to 2 attributes
fnExpandAll = lambda X: (list(fnExpand(x)) for x in X)
fnI = lambda x: sum(l[v] for v, l in zip(x, L))
fnMI = lambda x, y: sum((l[vx] + l[vy]) * (vx == vy) for vx, vy, l in zip(x, y, L))
fnMIC = lambda x, y: fnMI(x, y) / (fnI(x) + fnI(y))

class Model():
    def fnLoad(self,X):
        with self:
            V = (zip(*fnExpandAll(X))) # columns/categories
            C = list(co.Counter(c) for c in V) # attribute counts
            N = (sum(c.values()) for c in self.C) # precompute total
            L = list(dict((k, -1 * math.log(v / n)) for k, v in c.items()) for c, n in zip(C, N)) # information for each attribute

if __name__ == '__main__':
    with open("../../psych/16PF/data.csv") as f:
        cols = f.readline().strip().split('\t')
        X = list(line.strip().split('\t')[:] for line in f)[:1000]
    V = (zip(*fnExpandAll(X)))
    C = list(co.Counter(c) for c in V)
    N = (sum(c.values()) for c in C)
    L = list(dict((k,-1*math.log(v/n)) for k,v in c.items()) for c,n in zip(C,N))


# test
x = list(fnExpand(X[2]))
y = list(fnExpand(X[2]))
fnMIC(x,y) # similarity
s = list(fnMIC(x,y) for y in fnExpandAll(X)) # similarity vector
S = list(fnMIC(x,y) for x,y in it.combinations(fnExpandAll(X),2)) #similarity matrix as flat list
import matplotlib.pylab as plt
plt.hist(s,100)
plt.figure()
plt.hist(S,100)

