# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 22:47:08 2016

@author: johnlakness


Simple MNIST ANN with softmax activation and cross-entropy cost

"""
import numpy as np
import sklearn.datasets

mnist = sklearn.datasets.fetch_mldata('MNIST original',data_home="../data/")
X = mnist.data/255.0
Y = np.zeros((len(X),10))
Y[np.arange(len(Y)),mnist.target.astype(int)]=1

def fnF(x): return 1.0/(1+np.exp(-x)) #forward transformation
def fnD(f): return f*(1-f) # derivative df/dx as function of f
    
      
L = [X.shape[1],100,Y.shape[1]] #define layers by a list of integers [input, hidden, ... hidden, output]
W = [0.5*w/np.linalg.norm(w,2) for w in [np.random.uniform(-1.0,1.0,(L[i],L[i+1])) for i in range(len(L)-1)]]
for i in range(10):
    V = reduce( # forward propagation
        lambda V,w:V+[fnF(np.dot(V[-1],w))], # build list of post-synaptic outputs
        W, # weight matrices are transformations between layers
        [X] # start with the input data
        ) 
    DZ = reduce( #backprop to get pre-synaptic derivatives
        lambda D,(w,v):D+[np.dot(D[-1],w.T)*fnD(v)], # build list of presynaptic derivatives
        zip(W,V[:-1])[::-1], #list of (w,v) in reverse
        [(V[-1]-Y)] # pre-synaptic derivative of output layer
        )[::-1] # do operations in reverse and then re-reverse
    W = map( #expand derivatives to weight matrix and apply optimization
        lambda (w,v,dz): w-0.1*np.dot(v.T,dz)/len(v), # outer product and subtract
        zip(W,V[:-1],DZ[1:]) # weight,left post-synaptic outputs, right pre-synaptic derivatives
        ) 
    print(
        i, # iteration
        np.sum(np.equal(mnist.target,np.argmax(V[-1],axis=1)))*1.0/len(Y), # correct call fraction
        -np.sum(Y*np.log(V[-1]/np.sum(V[-1],axis=1)[:,None]))/len(Y), # cross entropy
        [np.linalg.norm(w,2) for w in W] # L2 norm of weights (largest possible pre-synaptic output)
        )


# google brain resident program
# aubrie@google.com