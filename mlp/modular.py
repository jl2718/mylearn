# -*- coding: utf-8 -*-
"""
Created on Thu Apr 28 00:21:04 2016

@author: johnlakness


transforms: X->W->fnMM->fnF->Y->fnC->fnDC->

"""
import numpy as np

# activation functions
def fnAF(x): return 1.0/(1+np.exp(-x)) #forward transformation
def fnAD(f): return f*(1-f) # derivative df/dx as function of f

#cost functions
def fnCF(v,y): return -np.sum(y*np.log(v/np.sum(v,axis=1)[:,None]))/len(y) #softmax cross-entropy
def fnCD(v,y): return (v-y) # derivative of cost function WRT presynaptic input

# transform functions
def fnXF(v,w): return np.dot(v,w) # forward transform between layers
def fnXDZ(d,w): return np.dot(d,w.T) # backprop transform between layers
def fnXDW(v,dz): return np.dot(v.T,dz)/len(v) # form derivatives of parameters
    
# Optimization function
def fnOpt(w,dw): return w-0.1*dw/np.linalg.norm(dw,2)

# evaluation function


import sklearn.datasets

mnist = sklearn.datasets.fetch_mldata('MNIST original',data_home="../data/")
X = mnist.data/255.0
Y = np.zeros((len(X),10))
Y[np.arange(len(Y)),mnist.target.astype(int)]=1

L = [X.shape[1],Y.shape[1]] #define layers by a list of integers [input, hidden, ... hidden, output]
W = [0.5*w/np.linalg.norm(w,2) for w in [np.random.uniform(-1.0,1.0,(L[i],L[i+1])) for i in range(len(L)-1)]]
for i in range(20):
    V = reduce( # forward propagation
        lambda V,w:V+[fnAF(fnXF(V[-1],w))], # build list of post-synaptic outputs
        W, # weight matrices are transformations between layers
        [X] # start with the input data
        ) 
    DZ = reduce( #backprop to get pre-synaptic derivatives
        lambda D,(w,v):D+[fnXDZ(D[-1],w)*fnAD(v)], # build list of presynaptic derivatives
        zip(W,V[:-1])[::-1], #list of (w,v) in reverse
        [fnCD(V[-1],Y)] # pre-synaptic derivative of output layer
        )[::-1] # do operations in reverse and then re-reverse
    W = map( #expand derivatives to weight matrix and apply optimization
        lambda (w,v,dz): fnOpt(w,fnXDW(v,dz)), # outer product and subtract
        zip(W,V[:-1],DZ[1:]) # weight,left post-synaptic outputs, right pre-synaptic derivatives
        ) 
    print(
        i, # iteration
        np.sum(np.equal(mnist.target,np.argmax(V[-1],axis=1)))*1.0/len(Y), # correct call fraction
        -np.sum(Y*np.log(V[-1]/np.sum(V[-1],axis=1)[:,None]))/len(Y), # cross entropy
        [np.linalg.norm(w,2) for w in W] # L2 norm of weights (largest possible pre-synaptic output)
        )
