# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 23:48:04 2016

Simple MLP

@author: johnlakness
"""

import numpy as np
import time

print "Running a simple test of my code for MLP vs Neon"

X = np.random.binomial(1,0.5,(1000,10))
Y = np.random.binomial(1,0.5,(len(X),1))

def fnF(x): return 1.0/(1+np.exp(-x)) #forward transformation
def fnD(f): return f*(1-f) # derivative df/dx as function of f
    
  
L = [X.shape[1],5,2,Y.shape[1]] #define layers by a list of integers [input, hidden, ... hidden, output]
W = [np.random.uniform(-1.0/L[i],1.0/L[i],(L[i],L[i+1])) for i in range(len(L)-1)] #Xavier initialization
DW = [0*w for w in W]
print "Okay, data all set up. Now timing my training loop:"
t=time.time()
for i in range(1000):
    V = reduce(lambda V,w:V+[fnF(np.dot(V[-1],w))],W,[X]) # feed-forward
    DZ = reduce(lambda D,(w,v):D+[np.dot(D[-1],w.T)*fnD(v)],zip(W,V[:-1])[::-1],[(fnD(V[-1])*(V[-1]-Y))])[::-1] #backprop to get pre-synaptic derivatives
    DW = [0.9*dw+0.1*np.dot(v.T,dz) for (dw,v,dz) in zip(DW,V[:-1],DZ[1:])] #momentum
    W = [w-0.1*dw for (w,dw) in zip(W,DW)] # weight update
#    print i,np.linalg.norm(V[-1]-Y,2),[np.linalg.norm(w,2) for w in W]
dt1 = time.time()-t
print "My training time:",dt1

print "Setting up Neon, wait a few seconds...."
import neon
import neon.util.argparser
import neon.initializers
import neon.layers
import neon.optimizers
import neon.models
import neon.transforms
import neon.callbacks

neon.backends.gen_backend(backend='cpu', batch_size=X.shape[0])
train = neon.data.ArrayIterator(X=X, y=Y,lshape=(1,1,X.shape[1]), make_onehot=False)
init_norm = neon.initializers.GlorotUniform() #Gaussian(loc=0.0, scale=0.01)
cost = neon.layers.GeneralizedCost(costfunc=neon.transforms.SumSquared())
optimizer = neon.optimizers.GradientDescentMomentum(0.1, momentum_coef=0.9)
activation = neon.transforms.Logistic()
layers = [neon.layers.Affine(nout=l,init=init_norm, bias=neon.initializers.Constant(0),activation=activation) for l in L[1:]]
mlp = neon.models.Model(layers=layers)
callbacks = neon.callbacks.callbacks.Callbacks(mlp,progress_bar=False)
#callbacks.add_callback(neon.callbacks.callbacks.RunTimerCallback())
print "Okay, ready now. Timing Neon:"
t=time.time()
mlp.fit(train, optimizer=optimizer, num_epochs=1000, cost=cost,callbacks=callbacks)
dt1 = time.time()-t
print "Neon's training time:",dt1