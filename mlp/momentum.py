# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 22:49:11 2016

@author: johnlakness
"""

import numpy as np
import time

X = np.random.binomial(1,0.5,(1000,10))
Y = np.random.binomial(1,0.5,(len(X),1))

def fnF(x): return 1.0/(1+np.exp(-x)) #forward transformation
def fnD(f): return f*(1-f) # derivative df/dx as function of f
    
  
L = [X.shape[1],5,2,Y.shape[1]] #define layers by a list of integers [input, hidden, ... hidden, output]
W = [np.random.uniform(-1.0/L[i],1.0/L[i],(L[i],L[i+1])) for i in range(len(L)-1)] #Xavier initialization
DW = [0*w for w in W]

for i in range(1000):
    V = reduce(lambda V,w:V+[fnF(np.dot(V[-1],w))],W,[X]) # feed-forward
    DZ = reduce(lambda D,(w,v):D+[np.dot(D[-1],w.T)*fnD(v)],zip(W,V[:-1])[::-1],[(fnD(V[-1])*(V[-1]-Y))])[::-1] #backprop to get pre-synaptic derivatives
    DW = [0.9*dw+0.1*np.dot(v.T,dz)/len(v) for (dw,v,dz) in zip(DW,V[:-1],DZ[1:])] #momentum
    W = [w-0.1*dw for (w,dw) in zip(W,DW)] # weight update
#    print i,np.linalg.norm(V[-1]-Y,2),[np.linalg.norm(w,2) for w in W]