# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 22:47:08 2016

@author: johnlakness
"""
import numpy as np
import sklearn.datasets

mnist = sklearn.datasets.fetch_mldata('MNIST original',data_home="../data/")
X = mnist.data/255.0
Y = np.zeros((len(X),10))
Y[np.arange(len(Y)),mnist.target.astype(int)]=1

def fnF(x): return 1.0/(1+np.exp(-x)) #forward transformation
def fnD(f): return f*(1-f) # derivative df/dx as function of f
    
      
L = [X.shape[1],Y.shape[1]] #define layers by a list of integers [input, hidden, ... hidden, output]
W = [0.5*w/np.linalg.norm(w,2) for w in [np.random.uniform(-1.0,1.0,(L[i],L[i+1])) for i in range(len(L)-1)]]
for i in range(10):
    V = reduce(lambda V,w:V+[fnF(np.dot(V[-1],w))],W,[X])
    DZ = reduce(lambda D,(w,v):D+[np.dot(D[-1],w.T)*fnD(v)],zip(W,V[:-1])[::-1],[(fnD(V[-1])*(V[-1]-Y))])[::-1] #backprop to get pre-synaptic derivatives
    W = [w-5.0*np.dot(v.T,dz)/(len(v)*np.linalg.norm(dz,2)) for (w,v,dz) in zip(W,V[:-1],DZ[1:])]
    print i,np.sum((V[-1]-Y)**2),[np.linalg.norm(w,2) for w in W]
