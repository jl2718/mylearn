# -*- coding: utf-8 -*-
"""
Created on Mon May  2 00:10:40 2016

@author: johnlakness
"""

import numpy as np

class Activation: # abstract class for basic transform
    def fnF(x): # forward propagation function
        raise NotImplementedError
    def fnD(y): # backprop derivative as a function of output
        raise NotImplementedError
        
class Logit(Activation):
    def fnF(x): return 1.0/(1+np.exp(-x))
    def fnD(f): return f*(1-f)